package org.pragmas.sms.smsrest

import android.net.wifi.WifiManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {

  private val btnStart by lazy {findViewById(R.id.btnStart) as Button }
  private val btnStop by lazy {findViewById(R.id.btnStop) as Button }
  private val logCat by lazy {findViewById(R.id.logCat) as TextView }
  private val labelHost by lazy {findViewById(R.id.labelHost) as TextView }
  private val labelPort by lazy {findViewById(R.id.labelPort) as TextView }

  override fun onCreate(savedInstanceState: Bundle?) {

    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

    labelHost.text = Utils.getIPAddress(
        this.getSystemService(AppCompatActivity.WIFI_SERVICE) as WifiManager
    ).joinToString(System.getProperty("line.separator"))
    labelPort.text = "9999"

    Logger.initialize(logCat)

    btnStart.setOnClickListener {
      RestServer.start()
    }
    btnStop.setOnClickListener {
      RestServer.stop()
    }
  }

  override fun onStart() {
    super.onStart()
    RestServer.start()
  }

  override fun onDestroy() {
    RestServer.stop()
    super.onDestroy()
  }

}
