package org.pragmas.sms.smsrest

import android.telephony.SmsManager
import com.google.gson.GsonBuilder
import fi.iki.elonen.NanoHTTPD
import fi.iki.elonen.NanoHTTPD.Response.Status.OK
import java.io.Serializable
import java.util.*

object CONTENT_TYPE {
  val json = "application/json"
  val html = "text/html"
  val css = "text/css"
  val js = "application/javascript"
  val jpeg = "image/jpeg"
  val png = "image/png"
  val ico = "image/x-icon"
}

object ROUTES {
  val assetsRoot = "/assets"
  val smsForm = "/sms/form"
  val smsSend = "/sms/send"
}

object STATUS {
  val success = "success"
  val failure = "failure"
}

object PARAMS {
  val phoneNumber = "phone_number"
  val smsText = "sms_text"
  val paramMethod = "method"
}

class RestServer(hostname: String?, port: Int) : NanoHTTPD(hostname, port) {

  private val ALLOWED_METHODS = "GET, POST" //, PUT, POST, DELETE, HEAD, OPTIONS"
  private val MAX_AGE = 1800

  companion object {

    var host = "0.0.0.0"
    var port = 9999
    var server: RestServer? = null
    var started = false

    fun start(p: Int = this.port) {
      if (server == null)
        server = RestServer(null, p)
      if (!started)
        server?.start(NanoHTTPD.SOCKET_READ_TIMEOUT, false)
      started = true
      Logger.i("Server", "start", "em")
    }

    fun stop() {
      server?.stop()
      server = null
      started = false
      Logger.i("Server", "stop", "em")
    }
  }

  private var parameters: Map<String, List<String>> = mutableMapOf()
  private var responseContentType = CONTENT_TYPE.json
  private val jsonProducer = GsonBuilder().setPrettyPrinting().create()
  private var currentSession: IHTTPSession? = null
  private val smsManager = SmsManager.getDefault()
  private val smsForm = FileManager.openAsString("assets/sms_form.html")

  fun addCORSHeaders(response: Response, cors: String): Response {
    response.addHeader("Access-Control-Allow-Origin", cors)
    response.addHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization")
    response.addHeader("Access-Control-Allow-Credentials", "true")
    response.addHeader("Access-Control-Allow-Methods", ALLOWED_METHODS)
    response.addHeader("Access-Control-Max-Age", MAX_AGE.toString())
    return response
  }

  private fun statusResponse(code: String, phone: String? = "", text: String? = ""): String {
    responseContentType = CONTENT_TYPE.json
    val timePoint = Date()
    val status = hashMapOf<String, Serializable?>(
        "status" to code,
        "phone" to phone,
        "text" to text,
        "date" to timePoint.toString()
    )
    return jsonProducer.toJson(status)
  }

  override fun serve(session: IHTTPSession): Response {
    val uri = session.uri
    val responseMsg: String = when (session.method.name) {
      "GET" -> when (uri) {
        ROUTES.smsForm -> {
          responseContentType = CONTENT_TYPE.html
          smsForm
        }
        else -> statusResponse(STATUS.failure, "", "404")
      }
      "POST" -> when (uri) {
        ROUTES.smsSend -> {
          var body: MutableMap<String, String> = mutableMapOf()
          session.parseBody(body)
          val methodFrom = parameters.get(PARAMS.paramMethod)
          val phoneNumber = session.parameters.get(PARAMS.phoneNumber)?.first()
          val smsText = session.parameters.get(PARAMS.smsText)?.first()
          try {
            smsManager.sendTextMessage(phoneNumber, null, smsText, null, null)
            Logger.i("SMS", "success to: $phoneNumber")
            methodFrom?.let {
              if (it.first() == "FORM") {
                responseContentType = CONTENT_TYPE.html
                smsForm
              } else {
                statusResponse(STATUS.failure, "", "method form not allowed")
              }
            } ?: statusResponse(STATUS.success, phoneNumber, smsText)
          } catch (e: Exception) {
            Logger.i("SMS", "failed to: $phoneNumber")
            statusResponse(STATUS.failure, phoneNumber, e.message)
          }
        }
        else -> statusResponse(STATUS.failure, "", "404")
      }
      else -> statusResponse(STATUS.failure, "", "404")
    }
    return addCORSHeaders(newFixedLengthResponse(OK, responseContentType, responseMsg), "*")
  }
}