package org.pragmas.sms.smsrest

import java.io.InputStream

object FileManager {

  fun open(filename: String) : InputStream =
    this.javaClass.classLoader.getResourceAsStream(filename)

  fun openAsString(filename: String) : String =
    this.javaClass.classLoader.getResourceAsStream(filename).bufferedReader().readText()

}
